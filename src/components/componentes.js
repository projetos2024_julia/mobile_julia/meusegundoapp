import { StyleSheet, Text, View, ScrollView, TextInput, Button } from 'react-native';
import { useState, useEffect } from "react";


export default function Componentes() {
    const [text, setText] = useState("");
    const [numLetras, setNumLetras] = useState(0);
    function click() {
        setNumLetras(text.length)
    }


    return (
        <View style={styles.container}>
            <Text style={styles.text}> Qual sua casa de Hogwarts? {text}</Text>
            <TextInput
                style={styles.input}
                placeholder='Digite algo..'
                value={text}
                onChangeText={(textInput) => setText(textInput)}
            />
            <Button
                title='Clique'
                onPress={() => {
                    click();
                }}
            />
            {
                numLetras > 0 ?
                    <Text> {numLetras} </Text>
                    :
                    <Text></Text>
            }
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    input: {
        borderWidth: 1,
        borderColor: 'gray',
        width: '80%',
        padding: 10,
        marginVertical: 10,
        backgroundColor: "white",
    }

});
