import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView } from 'react-native';



export default function LayoutGrade() {
    return (

        <View style={styles.container}>
            <View style={styles.linha}>
                <View style={styles.box1}></View>
                <View style={styles.box2}></View>
            </View>
            <View style={styles.linha}>
                <View style={styles.box3}></View>
                <View style={styles.box4}></View>
            </View>
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    linha: {
        flexDirection: "row",
    },
    box1: {
        width: 50,
        height: 50,
        backgroundColor: '#B682FF',
    },

    box2: {
        width: 50,
        height: 50,
        backgroundColor: '#FFABF7',
    },

    box3: {
        width: 50,
        height: 50,
        backgroundColor: '#62BBFF',
    },

    box4: {
        width: 50,
        height: 50,
        backgroundColor: '#B4F29B',
    },

});
