import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView } from 'react-native';



export default function LayoutHorizontal() {
    return (

        <View style={styles.container}>

            <View>
                <ScrollView horizontal={true}>
                    <View style={styles.box1}></View>
                    <View style={styles.box2}></View>
                    <View style={styles.box3}></View>
                    <View style={styles.box4}></View>
                    <View style={styles.box1}></View>
                    <View style={styles.box2}></View>
                    <View style={styles.box3}></View>
                    <View style={styles.box4}></View>
                    <View style={styles.box1}></View>
                    <View style={styles.box2}></View>
                    <View style={styles.box3}></View>
                    <View style={styles.box4}></View>
                    <View style={styles.box1}></View>
                    <View style={styles.box2}></View>
                    <View style={styles.box3}></View>
                    <View style={styles.box4}></View>
                    <View style={styles.box1}></View>
                    <View style={styles.box2}></View>
                    <View style={styles.box3}></View>
                    <View style={styles.box4}></View>
                    <View style={styles.box1}></View>
                    <View style={styles.box2}></View>
                    <View style={styles.box3}></View>
                    <View style={styles.box4}></View>
                </ScrollView>
            </View>
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "row",

    },

    box1: {
        width: 85,
        height: 85,
        backgroundColor: '#B682FF',
        borderRadius: 50,
        marginLeft: 5,
        marginTop: 5,
    },

    box2: {
        width: 85,
        height: 85,
        backgroundColor: '#FFABF7',
        borderRadius: 50,
        marginLeft: 5,
        marginTop: 5,
    },

    box3: {
        width: 85,
        height: 85,
        backgroundColor: '#62BBFF',
        borderRadius: 50,
        marginLeft: 5,
        marginTop: 5,
    },

    box4: {
        width: 85,
        height: 85,
        backgroundColor: '#B4F29B',
        borderRadius: 50,
        marginLeft: 5,
        marginTop: 5,
    },

});
