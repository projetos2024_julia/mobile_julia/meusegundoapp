import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView } from 'react-native';

export default function Layout() {
  return (

    <View style={styles.container}>


      <View style={styles.content}>
        <ScrollView>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>
          <Text style={styles.text}>conteudo</Text>

        </ScrollView>
      </View>

      <View style={styles.footer}>
        <Text style={styles.text}>rodape</Text>
      </View>
    </View>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: 'space-between',
  },
  header: {
    height: 35,
    backgroundColor: "#D0AEFF",
  },
  content: {
    marginTop: -265,
    flex: 1,
    backgroundColor: "#ACDBFF",
  },
  footer: {
    height: 50,
    backgroundColor: "#62BBFF",
  },
  text: {
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  }
});
